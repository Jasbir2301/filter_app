import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";

import MainScreen from "./mainScreen";

import allReducers from "../store/reducers";

const store = createStore(allReducers, applyMiddleware(thunkMiddleware));

const MainParent = () => {
  return (
    <Provider store={store}>
      <MainScreen />
    </Provider>
  );
};

export default MainParent;
