import React from "react";
import "./mainScreen.css";

import ListComponent from "../../components/listComponent";
import FilterComponent from "../../components/filterComponent";
import {
  loadJobsList,
  setSelectedFilter,
  setSearchQuery
} from "../../store/actions";
import { connect } from "react-redux";

const errorMessage = "Error in fetch, try reloading the page";
const loadingMessage = "Loading";
const noJobs = "No jobs found";

class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.handleInput = this.handleInput.bind(this);
    this.debounce = null;
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadJobsList());
  }
  handleInput(e) {
    this.props.dispatch(setSearchQuery(e.target.value.toLowerCase()));
  }
  handleSelect(event, filter) {
    this.props.dispatch(setSelectedFilter(filter, event.target.value));
  }
  render() {
    return (
      <div className="mainContainer">
        <div className="searchBarContainer">
          <span className="heading">Jobs Search</span>
          <input
            type="text"
            className="searchBar"
            placeholder="Search by keyword"
            onInput={this.handleInput}
          />
        </div>
        <FilterComponent
          filters={this.props.filters}
          handleSelect={(e, filter) => this.handleSelect(e, filter)}
        />
        <div className="countContainer">{`Found: ${this.props.allJobs.length} Jobs`}</div>
        <div className="listContainer">
          {this.props.allJobs.length === 0 && (
            <span className="loader">
              {this.props.hasError
                ? errorMessage
                : this.props.isLoading
                ? loadingMessage
                : noJobs}
            </span>
          )}
          <ListComponent allJobs={this.props.allJobs} />
        </div>
      </div>
    );
  }
}

function applyFilters(allJobs, filters) {
  const selectedExperience = filters.experience.selected;
  const selectedLocation = filters.location.selected;
  const searchQuery = filters.searchQuery;
  return allJobs.filter(job => {
    return (
      (job.experience === selectedExperience ||
        selectedExperience === "None") &&
      (job.location === selectedLocation || selectedLocation === "None") &&
      (job.title.toLowerCase().includes(searchQuery) ||
        job.companyname.toLowerCase().includes(searchQuery))
    );
  });
}

const mapStateToProps = state => {
  const allJobs = state.allJobs.get("jobsList").toJS();
  const filters = state.jobFilters.toJS();
  return {
    allJobs: applyFilters(allJobs, filters),
    isLoading: state.allJobs.get("isLoading"),
    length: state.allJobs.get("length"),
    filters: filters,
    hasError: state.allJobs.get("hasError")
  };
};

export default connect(mapStateToProps)(MainScreen);
