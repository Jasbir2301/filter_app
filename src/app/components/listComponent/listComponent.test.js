import React from "react";
import renderer from "react-test-renderer";

import ListComponent from "./index";

it("renders correctly when there are no items", () => {
  const tree = renderer.create(<ListComponent allJobs={[]} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it("renders correctly when there are some items", () => {
  const tree = renderer
    .create(
      <ListComponent
        allJobs={[
          {
            applylink:
              "https://www.techgig.com/jobs/Senior-Knowledge-Analyst-CKA/59843",
            companyname: "Boston Consultancy Group",
            created: "",
            enddate: "",
            experience: "4-6 yrs",
            jd: "",
            location: "Bengaluru/Bangalore",
            salary: "",
            skills: "cassandra",
            source: "techgig",
            startdate: "",
            timestamp: 1528959791.958316,
            title: "Senior Knowledge Analyst CKA",
            type: "",
            __v: 0,
            _id: "5b2b8a98263a5020388e87dc"
          }
        ]}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
