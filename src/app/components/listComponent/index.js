import React from "react";
import "./list.css";
const ListComponent = ({ allJobs }) => (
  <div className="list">
    {allJobs.map((job, jobIndex) => (
      <div key={jobIndex} className="job">
        <span className="companyName">{job.companyname}</span>
        <span>{job.title}</span>
        <span>{`posted on: ${(new Date(job.timestamp) + "").substring(
          0,
          15
        )}`}</span>
        <span>{`Location: ${job.location}`}</span>
        <span>{`Experience: ${job.experience}`}</span>
      </div>
    ))}
  </div>
);
export default ListComponent;
