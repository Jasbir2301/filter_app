import React from "react";
import renderer from "react-test-renderer";

import FilterComponent from "./index";

it("renders correctly when there are no filters", () => {
  const tree = renderer.create(<FilterComponent filters={{}} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it("renders correctly when there are some filters", () => {
  const tree = renderer
    .create(
      <FilterComponent
        filters={{
          experience: {
            categories: ["1", "2"],
            selected: "1"
          }
        }}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
