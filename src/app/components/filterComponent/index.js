import React from "react";
import "./filterComponent.css";
const FilterComponent = ({ filters, handleSelect }) => (
  <div className="filtersContainer">
    {Object.keys(filters).map(
      filter =>
        filter !== "searchQuery" && (
          <div className="singleFilter" key={filter}>
            <span>{filter.toUpperCase()}</span>
            <select
              className="selectBox"
              onChange={e => handleSelect(e, filter)}
            >
              {filters[filter].categories.map((category, categoryIndex) => (
                <option key={categoryIndex} value={category}>
                  {category}
                </option>
              ))}
            </select>
          </div>
        )
    )}
  </div>
);
export default FilterComponent;
