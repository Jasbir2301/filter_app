import { combineReducers } from "redux";
import Immutable from "immutable";

import actionConstants from "../actions/actionConstants";

function allJobs(
  state = new Immutable.Map({
    isLoading: false,
    jobsList: new Immutable.List(),
    length: 0,
    hasError: false
  }),
  action
) {
  switch (action.type) {
    case actionConstants.ADD_JOBS_LIST:
      return new Immutable.Map({
        isLoading: false,
        jobsList: new Immutable.List(action.payload.allJobsArray),
        length: action.payload.length,
        hasError: false
      });
    case actionConstants.LOAD_JOBS_LIST:
      return state.set("isLoading", true);
    case actionConstants.ERROR_IN_FETCH:
      return state.set("hasError", true);
    default:
      return state;
  }
}

function jobFilters(
  state = new Immutable.Map({
    location: new Immutable.Map({
      categories: new Immutable.List(["None"]),
      selected: "None"
    }),
    experience: new Immutable.Map({
      categories: new Immutable.List(["None"]),
      selected: "None"
    }),
    searchQuery: ""
  }),
  action
) {
  switch (action.type) {
    case actionConstants.GENERATE_CATEGORIES:
      let locationFilters = state.getIn(["location", "categories"]) || [];
      let experienceFilters = state.getIn(["experience", "categories"]) || [];
      action.payload.forEach(job => {
        if (job.location === "" || job.experience === "") return;
        if (!locationFilters.includes(job.location)) {
          locationFilters = locationFilters.push(job.location);
        }
        if (!experienceFilters.includes(job.experience)) {
          experienceFilters = experienceFilters.push(job.experience);
        }
      });
      let updatedState = state.setIn(
        ["location", "categories"],
        locationFilters
      );
      return updatedState.setIn(
        ["experience", "categories"],
        experienceFilters
      );
    case actionConstants.SET_SELECTED_FILTER:
      const { selectedValue, filterType } = action.payload;
      return state.setIn([filterType, "selected"], selectedValue);
    case actionConstants.SET_SEARCH_QUERY:
      return state.set("searchQuery", action.payload);
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  allJobs,
  jobFilters
});
export default rootReducer;
