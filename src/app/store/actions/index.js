import actionContants from "./actionConstants";
const corsAnywhere = "https://cors-anywhere.herokuapp.com/";
const jobsUrl = "https://nut-case.s3.amazonaws.com/jobs.json";

export function startLoadingJobs() {
  return {
    type: actionContants.LOAD_JOBS_LIST
  };
}
export function addJobsList(allJobsArray, length) {
  return {
    type: actionContants.ADD_JOBS_LIST,
    payload: {
      allJobsArray,
      length
    }
  };
}
function setCategories(allJobsArray) {
  return {
    type: actionContants.GENERATE_CATEGORIES,
    payload: allJobsArray
  };
}
export function setSelectedFilter(filterType, selectedValue) {
  return {
    type: actionContants.SET_SELECTED_FILTER,
    payload: {
      filterType,
      selectedValue
    }
  };
}
function errorInFetch() {
  return {
    type: actionContants.ERROR_IN_FETCH
  };
}
export function setSearchQuery(query) {
  return {
    type: actionContants.SET_SEARCH_QUERY,
    payload: query
  };
}
export function loadJobsList() {
  return dispatch => {
    dispatch(startLoadingJobs());
    fetch(corsAnywhere + jobsUrl)
      .then(res => res.json())
      .then(res => {
        if (res.data) {
          dispatch(addJobsList(res.data, res.len));
          dispatch(setCategories(res.data));
        }
      })
      .catch(err => dispatch(errorInFetch()));
  };
}
