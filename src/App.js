import React from "react";
import MyApp from "./app/screens";

function App() {
  return <MyApp />;
}

export default App;
