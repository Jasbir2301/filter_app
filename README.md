# Search Jobs

## Steps to run :-

#### `git clone git@bitbucket.org:Jasbir2301/filter_app.git`

#### `cd filter_app`

#### `npm i`

#### `npm start`

## To Test :-

#### `npm test`

Tests could have been more detailed and better covered.

## Assumptions :-

Project consumes existing API, where filter categorisation is not very distinct, so filters are generated in front-end.
Some filter categories could be handled better, like `Bangalore` / `Bengaluru` or `1 yr` / `1 yrs`, a RegEx soultion could be worked out to take care of such corner cases.
API error handling has been handled with a default error message.
